To install the project we need:
==============================

* Python 2.7+ (not 3+)
* pip
* virtualenv (not compulsory. I used it to install Django in a virtual environment)
* Django 1.7

Steps:      
1. Install python 2.7 (comes in built with OS X) and pip   
2. pip install virtualenv   
3. cd {Directory containing todoapp main project folder}   
4. virtualenv --no-site-packages ENV (you may give any name)   
5. source ENV/bin/activate   
6. pip install Django   
7. cd todoapp   
8. python manage.py migrate   
9. python manage.py runserver   

This will start the site in dev environment on your machine at port 8000