from django.conf.urls import patterns, url

from todoapp import views

urlpatterns = patterns('',
    url(r'^$', views.home, name='home'),
    url(r'^addlist/', views.addlist, name='addlist'),
    url(r'^(?P<tasklist_id>\d+)/$', views.listdetail, name='listdetail'),
    url(r'^(?P<tasklist_id>\d+)/addtask/', views.addtask, name='addtask'),
    url(r'^(?P<tasklist_id>\d+)/delete/', views.DeleteTaskList.as_view(), name='deletetasklist'),
    url(r'^(?P<tasklist_id>\d+)/edit/', views.edittasklist, name='edittasklist'),
    url(r'^(?P<tasklist_id>\d+)/(?P<task_id>\d+)/$', views.taskdetail, name='taskdetail'),
    url(r'^(?P<tasklist_id>\d+)/(?P<task_id>\d+)/delete/', views.DeleteTask.as_view(), name='deletetask'),
    url(r'^(?P<tasklist_id>\d+)/(?P<task_id>\d+)/edit/', views.edittask, name='edittask'),
)