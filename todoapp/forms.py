from django import forms
from todoapp.models import TaskList, Task
from django.contrib.admin import widgets 
from django.utils import timezone

class CreateTaskListForm(forms.Form):
	name = forms.CharField(max_length=200)

	def save(self, request):
		tasklist = TaskList(name=self.cleaned_data['name'], user=request.user)
		tasklist.save()

class CreateTaskForm(forms.Form):
	name = forms.CharField(max_length=200)
	description = forms.CharField(max_length=1000)
	expiration_date = forms.DateTimeField(widget = forms.DateTimeInput())

	def save(self, tasklist):
		task = Task(name=self.cleaned_data['name'], description=self.cleaned_data['description'],
			expiration_date=self.cleaned_data['expiration_date'], tasklist=tasklist)
		task.save()
	
class EditTaskListForm(forms.ModelForm):

	def edit(self):
		self.instance.name = self.cleaned_data['name']
		self.instance.save()

	class Meta:
		model = TaskList
		fields = ['name']
		ignore = ['user']


class EditTaskForm(forms.ModelForm):

	def edit(self):
		self.instance.name = self.cleaned_data['name']
		self.instance.description = self.cleaned_data['description']
		self.instance.expiration_date = self.cleaned_data['expiration_date']
		self.instance.save()

	class Meta:
		model = Task
		fields = ['name', 'description', 'expiration_date']
		ignore = ['tasklist']
