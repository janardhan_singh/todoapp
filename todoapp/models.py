from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

class TaskList(models.Model):
	name = models.CharField(max_length=200)
	user = models.ForeignKey(User)

	def __unicode__(self):
		return self.name

	def is_owned_by(self, user):
		return self.user == user


class Task(models.Model):
	name = models.CharField(max_length=200)
	description = models.CharField(max_length=1000)
	expiration_date = models.DateTimeField()
	tasklist = models.ForeignKey(TaskList)

	def __unicode__(self):
		return self.name

	def isActive(self):
		return self.expiration_date > timezone.now()

	def is_owned_by(self, user):
		return self.tasklist.is_owned_by(user)

	def get_info(self):
		if self.isActive():
			return "Active"
		else:
			return "Expired"

