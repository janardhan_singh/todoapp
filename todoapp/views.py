from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect, Http404
from django.contrib.auth.decorators import login_required
from todoapp.models import TaskList, Task
from todoapp.forms import *
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy

@login_required
def home(request):
	list_of_tasklist = TaskList.objects.filter(user=request.user)
	return render(request, 'todoapp/home.html', {'list': list_of_tasklist, 'base_url': '/todoapp/'})


@login_required
def addlist(request):
	if request.method == "POST":
		form = CreateTaskListForm(request.POST)
		if form.is_valid():
			tasklist = form.save(request)
			return HttpResponseRedirect('/todoapp/')
	else:
		form = CreateTaskListForm()
	return render(request, 'todoapp/addlist.html', {'form': form})

@login_required
def addtask(request, tasklist_id):
	if request.method == "POST":
		form = CreateTaskForm(request.POST)
		tasklist = TaskList.objects.get(pk=tasklist_id)
		if form.is_valid():
			task = form.save(tasklist)
			return HttpResponseRedirect('/todoapp/'+tasklist_id+'/')
	else:
		form = CreateTaskForm()
	return render(request, 'todoapp/addtask.html', {'form': form})


@login_required
def listdetail(request, tasklist_id):
	tasklist = TaskList.objects.get(pk=tasklist_id)
	if tasklist is not None and tasklist.is_owned_by(request.user):
		tasks = Task.objects.filter(tasklist=tasklist)
		return render(request, 'todoapp/tasks.html', {'list': tasks, 'base_url': '/todoapp/'+tasklist_id+'/', 
			'tasklist': tasklist})
	return HttpResponseNotFound('<h1>Page not found</h1>')

@login_required
def taskdetail(request, tasklist_id, task_id):
	task = Task.objects.get(pk=task_id)
	print task.is_owned_by(request.user)
	if (task is not None) and (int(task.tasklist.id) == int(tasklist_id)) and (task.is_owned_by(request.user)):
		status = 'Expired'
		if task.isActive():
			status = 'Active'
		return render(request, 'todoapp/taskdetail.html', {'task': task, 'status': status,
			'base_url': '/todoapp/'+ str(tasklist_id) + '/' + str(task.id)})
	return HttpResponseNotFound('<h1>Page not found</h1>')

@login_required
def edittasklist(request, tasklist_id):
	tasklist = TaskList.objects.get(pk=tasklist_id)
	if tasklist is not None and tasklist.is_owned_by(request.user):
		if request.method == "POST":
			form = EditTaskListForm(request.POST, instance=tasklist)
			if form.is_valid():
				form.edit()
				return HttpResponseRedirect('/todoapp/')
		else:
			form = EditTaskListForm(instance=tasklist)
		return render(request, 'todoapp/edittasklist.html', {'form': form})
	return HttpResponseNotFound('<h1>Page not found</h1>')

@login_required
def edittask(request, tasklist_id, task_id):
	task = Task.objects.get(pk=task_id)
	if task is not None and int(task.tasklist.id) == int(tasklist_id) and task.is_owned_by(request.user):
		if request.method == "POST":
			form = EditTaskForm(request.POST, instance=task)
			if form.is_valid():
				form.edit()
				return HttpResponseRedirect('/todoapp/'+tasklist_id+'/' + task_id)
		else:
			form = EditTaskForm(instance=task)
		return render(request, 'todoapp/edittask.html', {'form': form})
	return HttpResponseNotFound('<h1>Page not found</h1>')


class LoginRequiredMixin(object):
	@classmethod
	def as_view(cls, **initkwargs):
		view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
		return login_required(view)


class DeleteTaskList(LoginRequiredMixin, DeleteView):
	model = TaskList
	success_url = reverse_lazy('home')
	template_name = 'todoapp/delete_confirm.html'

	def get_object(self):
		tasklist_id = self.kwargs['tasklist_id']
		tasklist = TaskList.objects.get(pk=tasklist_id)
		if tasklist is not None and tasklist.is_owned_by(self.request.user):
			return tasklist
		raise Http404


class DeleteTask(LoginRequiredMixin, DeleteView):
	model = Task
	template_name = 'todoapp/delete_confirm.html'

	def get_object(self):
		self.task_id = self.kwargs['task_id']
		self.tasklist_id = self.kwargs['tasklist_id']
		self.success_url = '/todoapp/' + self.tasklist_id + "/"
		task = Task.objects.get(pk=self.task_id)
		if task is not None and task.is_owned_by(self.request.user):
			return task
		raise Http404






