from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import AuthenticationForm
from forms import CreateUserForm
from django.contrib.auth.models import User
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from todoapp.models import TaskList, Task


def home(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect("/todoapp/")
	else:
		return HttpResponseRedirect("/login/")

def logout_user(request):
	logout(request)
	return HttpResponseRedirect("/login/")

def createuser(request):
	if request.method == "POST":
		form = CreateUserForm(request.POST)
		if form.is_valid():
			new_user = CreateUserForm.save(form)
			if new_user is not None:
				authenticated_user = authenticate(username=request.POST['username'], password=request.POST['password1'])
				if authenticated_user is not None:
					print "User authenticated"
					login(request, authenticated_user)
					print "user logged in"

					'''Add permissions'''
					content_type = ContentType.objects.get_for_model(TaskList)
					permissions = Permission.objects.filter(content_type=content_type)
					for permission in permissions:
						authenticated_user.user_permissions.add(permission)
					content_type = ContentType.objects.get_for_model(Task)
					permissions = Permission.objects.filter(content_type=content_type)
					for permission in permissions:
						authenticated_user.user_permissions.add(permission)
					return HttpResponseRedirect('/todoapp/')
				else :
					print "user could not be authenticated"
			else:
				print "Error"
		else:
			print "Form invalid"
	else:
		print "print blank form"
		form = CreateUserForm()
	return render(request, 'registration/adduser.html', {'form': form}) 
