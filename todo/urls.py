from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'todo.views.home', name='home'),
    url(r'^login/', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/', 'todo.views.logout_user', name='logout_user'),
    url(r'^createuser/', 'todo.views.createuser', name="createuser"),
    url(r'^todoapp/', include('todoapp.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
